<?php

namespace becontent\system;

use becontent\resource\entity\Resource as Resource;

class User extends Resource {
	
	protected $name = "";
	protected $surname = "";
	
	protected $username;
	
	protected $password;
	
	/**
	 * @rel becontent\system\Group
	 * @multiplicity ManyToMany
	 * @mappedBy users
	 */
	protected $groups;
	
	/**
	 *
	 * @param unknown $name        	
	 * @param unknown $value        	
	 */
	public function __set($name, $value) {
		$callParent = true;
		if ($name == "password") {
			$value = md5 ( $value );
		}
		return parent::__set ( $name, $value );
	}
	
	/**
	 *
	 * @param unknown $name        	
	 * @return string
	 */
	public function __get($name) {
		return parent::__get ( $name );
	}
	
	/**
	 *
	 * @param unknown $password        	
	 */
	public function doLogin($md5OfPassword) {
		if ($md5OfPassword == $this->password || md5($md5OfPassword) == $this->password) {
			$this->loggedIn = true;
		}
		return $this->loggedIn;
	}
}
?>