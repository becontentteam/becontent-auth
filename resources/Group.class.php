<?php
namespace becontent\system;
use becontent\resource\entity\Resource as Resource;
class Group extends Resource
{
	protected $name;
	
	/**
	 * 
	 * @rel becontent\system\User
	 * @multiplicity ManyToMany
	 * @inversedBy groups
	 * 
	 */
	protected $users;
}