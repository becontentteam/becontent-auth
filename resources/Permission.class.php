<?php

namespace becontent\system;

use becontent\resource\entity\Resource as Resource;

class Permission extends Resource {
	
	/**
	 * @volatile
	 */
	public static $writePermission = "permission_write";
	
	/**
	 * @volatile
	 */
	public static $readPermission = "permission_read";
	
	/**
	 * @volatile
	 */
	public static $execPermission = "permission_exec";
	
	/**
	 *
	 * @var boolean
	 */
	protected $write;
	
	/**
	 *
	 * @var boolean
	 */
	protected $read;
	
	/**
	 *
	 * @var boolean
	 */
	protected $execute;
	
	/**
	 * @rel becontent\system\Group
	 * @multiplicity OneToOne
	 */
	protected $group;
	
	/**
	 * The classifier of the resource that requires permission
	 *
	 * @var string
	 */
	protected $authorizingResourceClassifier;
	
	/**
	 * 
	 * @param unknown $group
	 * @param unknown $resource
	 * @param unknown $operation
	 */
	public function authorizeGroup($group, $resource, $operation) {
		$authorized = false;
		
		if ($resource->resClassifier == $this->authorizingResourceClassifier) {
			
			if ($this->group->id == $group->id) {
				
				if ($operation == self::$read && $this->read) {
					$authorized = true;
				}
				if ($operation == self::$write && $this->write) {
					$authorized = true;
				}
				if ($operation == self::$exec && $this->exec) {
					$authorized = true;
				}
				break;
			}
		}
	}
	
	/**
	 * 
	 * @param unknown $user
	 * @param unknown $resource
	 * @param unknown $operation
	 * @return boolean
	 */
	public function authorizeUser($user, $resource, $operation) {
		$authorized = false;
		
		if ($resource->resClassifier == $this->authorizingResourceClassifier) {
			
			if (isset ( $user->groups )) {
				
				foreach ( $user->groups as $k => $v ) {
					
					if ($this->group->id == $v->id) {
						
						if ($operation == self::$read && $this->read) {
							$authorized = true;
						}
						if ($operation == self::$write && $this->write) {
							$authorized = true;
						}
						if ($operation == self::$exec && $this->exec) {
							$authorized = true;
						}
						break;
					}
				}
			}
		}
		return $authorized;
	}
}