<?php

namespace becontent\auth\control;

use becontent\router\Router as Router;
use becontent\core\control\Settings as Settings;

abstract class ResourceController {
	
	/**
	 * The identifier for the controllerstable for router
	 *
	 * @var unknown
	 */
	protected $routingIdentifier;
	
	/**
	 * Realization of interface AuthCallback called by the controller
	 * in case of authentication success
	 *
	 * @var unknown
	 */
	private $authenticationSuccessCallback;
	
	/**
	 * Realization of interface AuthCallback called by the controller
	 * in case of authentication failure
	 *
	 * @var unknown
	 */
	private $authenticationFailureCallback;
	
	/**
	 * Realization of interface AuthCallback called by the controller
	 * in case of authorization success
	 *
	 * @var unknown
	 */
	private $authorizationSuccessCallback;
	
	/**
	 * Realization of interface AuthCallback called by the controller
	 * in case of authorization failure
	 *
	 * @var unknown
	 */
	private $authorizationFailureCallback;
	
	/**
	 *
	 * @var unknown
	 */
	private $requiresAuthorization = true;
	
	/**
	 *
	 * @var unknown
	 */
	private $requiresAuthentication = true;
	
	/**
	 *
	 * @var unknown
	 */
	private $authenticationRequest;
	
	/**
	 *
	 * @var unknown
	 */
	private $authorizationRequest;
	
	/**
	 *
	 * @var unknown
	 */
	private $incomingData;
	
	/**
	 *
	 * @var unknown
	 */
	private $outgoingData;
	
	/**
	 *
	 * @var unknown
	 */
	private $operation;
	
	/**
	 *
	 * @var unknown
	 */
	private $subject;
	private $loggedUser;
	
	/**
	 */
	public function __construct($requiresAuthorization = true, $requiresAuthentication = true) {
		$this->requiresAuthorization = $requiresAuthorization;
		
		/**
		 * Authorization implicitly requires authentication
		 */
		if (! $this->requiresAuthorization) {
			$this->requiresAuthentication = $requiresAuthentication;
		}
		
		/**
		 */
		$controllersTable = array (
				$this->routingIdentifier => $this 
		);
		/**
		 */
		Router::getInstance ()->injectControllersTable ( $controllersTable );
	}
	public function getRoutingIdentifier() {
		return $this->routingIdentifier;
	}
	
	/**
	 *
	 * @param unknown $authenticationSuccessCallback        	
	 */
	public function setOnAuthenticationSuccess($authenticationSuccessCallback) {
		$this->authenticationSuccessCallback = $authenticationSuccessCallback;
	}
	
	/**
	 *
	 * @param unknown $authenticationFailureCallback        	
	 */
	public function setOnAuthenticationFailure($authenticationFailureCallback) {
		$this->authenticationFailureCallback = $authenticationFailureCallback;
	}
	
	/**
	 *
	 * @param unknown $authenticationSuccessCallback        	
	 */
	public function setOnAuthorizationSuccess($authorizationSuccessCallback) {
		$this->authorizationSuccessCallback = $authorizationSuccessCallback;
	}
	
	/**
	 *
	 * @param unknown $authenticationFailureCallback        	
	 */
	public function setOnAuthorizationFailure($authorizationFailureCallback) {
		$this->authorizationFailureCallback = $authorizationFailureCallback;
	}
	public function setIncomingData($incomingData) {
		$this->incomingData = $incomingData;
	}
	public function getIncomingData() {
		return $this->incomingData;
	}
	public function getOutgoingData() {
		return $this->outgoingData;
	}
	
	/**
	 * Must return a correct authentication request to be passed to the Authority object
	 */
	public function buildAuthenticationRequest() {
		return new AuthenticationRequest ();
	}
	
	/**
	 * Must return a correct authorization request to be passed to the Authority object
	 */
	public function buildAuthorizationRequest() {
		return new AuthorizationRequest ();
	}
	public function getLoggedUser() {
		$authority = Authority::getInstance ();
		$authority->getLoggedUser ();
		return $authority->getLoggedUser ();
	}
	/**
	 * It must implement operations required after the authorization
	 */
	abstract public function executeOperations();
	public function doTheMagic() {
		$clearToProceed = false;
		$authenticationDone = false;
		$authorizationDone = false;
		/**
		 */
		if (! $this->requiresAuthorization && ! $this->requiresAuthentication) {
			$clearToProceed = true;
		} else {
			/**
			 * Instantiation of authority
			 */
			$authority = Authority::getInstance ();
			/**
			 * Building authenticator with custom configurations
			 */
			$authority->rebuildAuthenticator ( $this->buildAuthenticationRequest () );
			
			if ($authority->authenticate ( $this->authenticationSuccessCallback, $this->authenticationFailureCallback )) {
				
				if ($this->requiresAuthorization) {
					/**
					 * Building authorizer with custom configurations
					 */
					$authority->rebuildAuthorizer ( $this->buildAuthorizationRequest () );
					if ($authority->authorize ( $this->authorizationSuccessCallback, $this->authorizationFailureCallback )) {
						$clearToProceed = true;
					}
				} else {
					$clearToProceed = true;
				}
			}
		}
		
		if ($clearToProceed) {
			$this->executeOperations ();
		}
	}
}