<?php
namespace becontent\auth\control;

class AuthenticationRequest {
	/**
	 * 
	 * @var unknown
	 */
	private $authenticationMode;
	
	/**
	 * 
	 * @var unknown
	 */
	private $userClassifier;
	
	/**
	 * 
	 * @var unknown
	 */
	private $groupClassifier;
	
	/**
	 * 
	 * @var unknown
	 */
	private $userIdentifier;
	
	/**
	 * 
	 * @var unknown
	 */
	private $userSecurity;
	
	/**
	 * 
	 */
	public function __construct()
	{
		$this->authenticationMode=AuthenticationSettings::$defaultAuthenticatorMode;
		$this->userClassifier=AuthenticationSettings::$authenticationUserResourceClassifier;
		$this->groupClassifier=AuthenticationSettings::$authenticationGroupResourceClassifier;
		$this->userIdentifier=AuthenticationSettings::$authenticationUserIdentificationProperty;
		$this->userSecurity=AuthenticationSettings::$authenticationUserSecurityParameter;
	}
	
	public function setAuthenticationMode($mode){
		$this->authenticationMode=$mode;
	}
	
	public function setUserClassifier($uClass)
	{
		$this->userClassifier=$uClass;
	}
	
	public function setGroupClassifier($gClass)
	{
		$this->groupClassifier=$gClass;
	}
	
	public function setUserIdentifier($uID)
	{
		$this->userIdentifier=$uID;
	}
	
	public function setUserSecurity($uSec){
		$this->userSecurity=$uSec;
	}
	
	public function getAuthenticationMode(){
		return $this->authenticationMode;
	}
	
	public function getUserClassifier()
	{
		return $this->userClassifier;
	}
	
	public function getGroupClassifier()
	{
		return $this->groupClassifierr;
	}
	
	public function getUserIdentifier()
	{
		return $this->userIdentifier;
	}
	
	public function getUserSecurity(){
		return $this->userSecurity;
	}
	
}