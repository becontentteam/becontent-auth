<?php
namespace becontent\auth\control;

class AuthenticationSettings {
	
	/**
	 * The resource classifier of the Resource Specialization used to do authorization
	 * It must extend the becontent\system\User class and must implement/reimplement the static method
	 * authorize($md5OfPassword)
	 *
	 * The default one is imposed in becontent\core\control\Settings class and can be changed respecting
	 * the previous constraints
	 *
	 * @var unknown
	 */
	public static $authenticationGroupResourceClassifier = "becontent\system\Group";
	
	/**
	 * The resource classifier of the Resource Specialization used to do authorization
	 * It must extend the becontent\system\User class and must implement/reimplement the static method
	 * authorize($md5OfPassword)
	 *
	 * The default one is imposed in becontent\core\control\Settings class and can be changed respecting
	 * the previous constraints
	 * 
	 * @var unknown
	 */
	public static $authenticationUserResourceClassifier = "becontent\system\User";
	
	/**
	 * The name of the property of the Resource Specialization used to identify the user in the system
	 * It must be set accordingly with the selected Resource Specialization imposed in above
	 * $authenticationUserResourceClassifier in order to retrieve user
	 * 
	 * @var unknown
	 */
	public static $authenticationUserIdentificationProperty = "username";
	
	/**
	 * The name of the property of the Resource Specialization used to do authorization (password as example)
	 * It must be set accordingly with the selected Resource Specialization imposed in above
	 * $authenticationUserResourceClassifier in order to retrieve user
	 * 
	 * @var unknown
	 */
	public static $authenticationUserSecurityParameter = "password";
	
	
	public static $defaultAuthenticatorMode = "GETMODE";
}