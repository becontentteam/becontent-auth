<?php
namespace becontent\auth\control;

class AuthoritySuccessCallback implements AuthCallback{
	
	public function callback($result){
		
		
	}
	
}

class AuthorityFailureCallback implements AuthCallback{

	public function callback($result){


	}

}

class Authority
{
	private $authenticator;
	
	private $authorizer;
	
	public static $instance;
	
	public static function getInstance()
	{
		if(!isset(self::$instance))
		{
			self::$instance=new Authority();
		}
		
		return self::$instance;
	}
	
	private function __construct()
	{
		$this->authenticator=new Authenticator(AuthenticationSettings::$defaultAuthenticatorMode);
		
		/**
		 * Work in progresss XD
		 */
		$this->authorizer=null; 
	}
	
	public function rebuildAuthenticator($authenticationRequest)
	{
		AuthenticationSettings::$defaultAuthenticatorMode=$authenticationRequest->getAuthenticationMode();
		AuthenticationSettings::$authenticationGroupResourceClassifier=$authenticationRequest->getGroupClassifier();
		AuthenticationSettings::$authenticationUserResourceClassifier=$authenticationRequest->getUserClassifier();
		AuthenticationSettings::$authenticationUserIdentificationProperty=$authenticationRequest->getUserIdentifier();
		AuthenticationSettings::$authenticationUserSecurityParameter=$authenticationRequest->getUserSecurity();
		$this->authenticator=new Authenticator(AuthenticationSettings::$defaultAuthenticatorMode);
	}
	
	public function rebuildAuthorizer($authorizationRequest)
	{
		/**
		 * Work in progress XD
		 */
		$this->authorizer=null;
	}
	
	public function authenticate($successCallback, $failureCallback)
	{
		return $this->authenticator->authenticate($successCallback, $failureCallback);
	}
	
	
	public function authorize($authorizationRequest,$successCallback, $failureCallback)
	{
		
		/**
		 * WORK IN PROGRESS
		 */
		return true;
	}
	
	
	public function getLoggedUser()
	{
		return $this->authenticator->getLoggedUser();
		
	}
}