<?php
namespace becontent\auth\control;

class AuthorizationSettings {
	
	/**
	 * The resource classifier of the Resource Specialization used to do authorization
	 * It must extend the becontent\system\User class and must implement/reimplement the static method
	 * authorize($md5OfPassword)
	 *
	 * The default one is imposed in becontent\core\control\Settings class and can be changed respecting
	 * the previous constraints
	 *
	 * @var unknown
	 */
	public static $authorizationGroupResourceClassifier = "becontent\system\Group";
	
	/**
	 * The resource classifier of the Resource Specialization used to do authorization
	 * It must extend the becontent\system\User class and must implement/reimplement the static method
	 * authorize($md5OfPassword)
	 *
	 * The default one is imposed in becontent\core\control\Settings class and can be changed respecting
	 * the previous constraints
	 * 
	 * @var unknown
	 */
	public static $authorizationUserResourceClassifier = "becontent\system\User";
	
	/**
	 * The resource classifier of the Resource Specialization used to do authorization
	 * It must extend the becontent\system\User class and must implement/reimplement the static method
	 * authorize($md5OfPassword)
	 *
	 * The default one is imposed in becontent\core\control\Settings class and can be changed respecting
	 * the previous constraints
	 *
	 * @var unknown
	 */
	public static $authorizationPermissionResourceClassifier = "becontent\system\Permission";
	
	/**
	 * The name of the property of the Resource Specialization used to identify the user in the system
	 * It must be set accordingly with the selected Resource Specialization imposed in above
	 * $authorizationUserResourceClassifier in order to retrieve user
	 * 
	 * @var unknown
	 */
	public static $authorizationUserIdentificationProperty = "username";
	
	/**
	 * The name of the property of the Resource Specialization used to identify the user in the system
	 * It must be set accordingly with the selected Resource Specialization imposed in above
	 * $authorizationUserResourceClassifier in order to retrieve user
	 *
	 * @var unknown
	 */
	public static $authorizationGroupIdentificationProperty = "group";
	
	
	public static $defaultAuthorizerMode = "GETMODE";
}