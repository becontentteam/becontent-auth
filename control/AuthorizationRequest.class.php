<?php
namespace becontent\auth\control;

class AuthorizationRequest {
	
	private $authorizationMode;
	
	private $userClassfier;
	
	private $groupClassifier;
	
	private $permissionClassifier;
	
	private $operation;
	
	private $userIdentifier;
	
	private $groupIdentifier;
	
	
	
	private function __construct($operation)
	{
		$this->authorizationMode=AuthorizationSettings::$defaultAuthorizationMode;
		$this->userClassifier=AuthentorizationSettings::$authorizationUserResourceClassifier;
		$this->groupClassifier=AuthentorizationSettings::$authorizationGroupResourceClassifier;
		$this->permissionClassifier=AuthentorizationSettings::$authorizationPermissionResourceClassifier;
		$this->userIdentifier=AuthentorizationSettings::$authorizationUserIdentificationProperty;
		$this->groupIdentifier=AuthentorizationSettings::$authorizationGroupIdentificationProperty;
		$this->operation=$operation;
	}
	
}