<?php
namespace becontent\auth\control;
interface AuthCallback{
	
	public function callback($result);
}