<?php

namespace becontent\auth\control;

use becontent\resource\entity\Resource as Resource;
use becontent\beContent as beContent;
use becontent\core\control\Settings as Settings;

/**
 * Official authentication class for the becontent framework
 * It uses cookies or webstorage
 */
class Authenticator {
	
	/**
	 * Enumerator used to identify GET fetching mode (parameters fetched from $_GET)
	 *
	 * @var string
	 */
	private static $getMode = "GETMODE";
	/**
	 * Enumerator used to identify GET fetching mode (parameters fetched from $_GET)
	 *
	 * @var string
	 */
	private static $postMode = "POSTMODE";
	/**
	 * Enumerator used to identify GET fetching mode (parameters fetched from $_GET)
	 *
	 * @var string
	 */
	private static $cookiesMode = "COOKIESMODE";
	
	/**
	 * Enumerator used to identify GET fetching mode (parameters fetched from $_GET)
	 *
	 * @var string
	 */
	private static $sessionMode = "SESSIONMODE";
	
	
	
	
	private $actualMode = "COOKIESMODE";
	private $userIdentifier;
	private $userSecurity;
	private $loggedUser = null;
	private $authenticated;
	private $alreadyInSession;
	
	
	
	/**
	 * The resource classifier of the Resource Specialization used to do authorization
	 * It must extend the becontent\system\User class and must implement/reimplement the static method
	 * authorize($md5OfPassword)
	 *
	 * The default one is imposed in becontent\core\control\Settings class and can be changed respecting
	 * the previous constraints
	 *
	 * @var string
	 */
	private $authenticationUserResourceClassifier;
	
	/**
	 * The name of the property of the Resource Specialization used to identify the user in the system
	 * It must be set accordingly with the selected Resource Specialization imposed in above
	 * $authenticationUserResourceClassifier in order to retrieve user
	 *
	 * @var string
	 */
	private $authenticationUserIdentificationProperty;
	
	/**
	 * The name of the property of the Resource Specialization used to do authorization (password as example)
	 * It must be set accordingly with the selected Resource Specialization imposed in above
	 * $authenticationUserResourceClassifier in order to retrieve user
	 *
	 * @var string
	 */
	private $authenticationUserSecurityParameter;
	
	/**
	 * Constructor to be used in router
	 *
	 * @param unknown $mode        	
	 */
	public function __construct($mode) {
		
		/**
		 * Initializing parameters with the latest settings
		 */
		$this->authenticationUserResourceClassifier = AuthenticationSettings::$authenticationUserResourceClassifier;
		$this->authenticationUserIdentificationProperty = AuthenticationSettings::$authenticationUserIdentificationProperty;
		$this->authenticationUserSecurityParameter = AuthenticationSettings::$authenticationUserSecurityParameter;
		
		/**
		 * selecting the fetching mode
		 */
		$this->actualMode = $mode;
		
		/**
		 * Fetching infos from the environment
		 */
		if ($this->actualMode == self::$sessionMode) {
			
			$this->userIdentifier = $_SESSION [$this->authenticationUserIdentificationProperty];
			
			$this->userSecurity = $_SESSION [$this->authenticationUserSecurityParameter];
		} else if ($this->actualMode == self::$cookiesMode) {
			
			$this->userIdentifier = $_COOKIE [$this->authenticationUserIdentificationProperty];
			
			$this->userSecurity = $_COOKIE [$this->authenticationUserSecurityParameter];
		} else if ($this->actualMode == self::$getMode) {
			
			$this->userIdentifier = $_GET [$this->authenticationUserIdentificationProperty];
			
			$this->userSecurity = $_GET [$this->authenticationUserSecurityParameter];
		} else if ($this->actualMode == self::$postMode) {
			
			$this->userIdentifier = $_POST [$this->authenticationUserIdentificationProperty];
			
			$this->userSecurity = $_POST [$this->authenticationUserSecurityParameter];
		}
	}
	
	/**
	 *
	 * @param unknown $successCallback        	
	 * @param unknown $failureCallback        	
	 */
	public function authenticate($successCallback, $failureCallback) {
		$this->authenticated = false;
		$reason = "unknown error";
		$this->alreadyInSession = false;
		
		/**
		 * Check if already in session
		 */
		session_start ();
		
		if (isset ( $_SESSION ["alreadyLoggedIn"] )) {
			if (isset ( $this->userIdentifier ) && isset ( $this->userSecurity )) {
				if ($this->userIdentifier == $_SESSION [$this->authenticationUserIdentificationProperty] && $this->userSecurity = $_SESSION [$this->authenticationUserSecurityParameter]) {
					$this->alreadyInSession = true;
				}
			} else {
				$this->alreadyInSession = true;
				$this->userIdentifier = $_SESSION [$this->authenticationUserIdentificationProperty];
				$this->userSecurity = $_SESSION [$this->authenticationUserSecurityParameter];
			}
		}
		
		if (! $this->alreadyInSession) {
			session_regenerate_id ( true );
		}
		
		if ($this->alreadyInSession && isset ( $this->loggedUser )) {
			
			$this->authenticated = true;
		} else {
			
			/**
			 * preparing the user research
			 */
			$query_conditions = array (
					$this->authenticationUserIdentificationProperty => $this->userIdentifier 
			);
			/**
			 * querying resources
			 */
			$requestedResources = beContent::getInstance ()->findResources ( $this->authenticationUserResourceClassifier, $query_conditions );
			
			/**
			 * executing LogIn
			 */
			if (is_array ( $requestedResources ) && count ( $requestedResources ) > 0 && count ( $requestedResources ) < 2) {
				if ($requestedResources [0]->doLogin ( $this->userSecurity )) {
					$this->authenticated = true;
					$this->loggedUser = $requestedResources [0];
					$reason = "'cause becontent is strong enough";
				} else {
					$reason = "passord incorrect";
				}
			} else {
				$reason = "user not found";
			}
			
			/**
			 * Building result bundle for callback
			 */
			$result = array (
					"result" => $this->authenticated,
					"reason" => $reason,
					"requestedUser" => $this->loggedUser 
			);
		}
		/**
		 * Invoking callbacks
		 */
		if ($this->authenticated) {
			$_SESSION [$this->authenticationUserIdentificationProperty] = $this->userIdentifier;
			$_SESSION [$this->authenticationUserSecurityParameter] = $this->userSecurity;
			$_SESSION ["alreadyLoggedIn"] = true;
			if (isset ( $successCallback ))
				$successCallback->callback ( $result );
		} else {
			session_regenerate_id ( true );
			session_destroy ();
			if (isset ( $failureCallback ))
				$failureCallback->callback ( $result );
		}
		return $this->authenticated;
	}
	
	public function getLoggedUser()
	{
		return $this->loggedUser;
	}
}


